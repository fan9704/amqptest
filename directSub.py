import os
import pika
import sys


credentials = pika.PlainCredentials('guest', 'guest') # mq⽤户名和密码
# BlockingConnection:同步模式
connection = pika.BlockingConnection(pika.ConnectionParameters(
    host=os.getenv("AMQP_SERVER_IP",'localhost'),
    port=os.getenv("AMQP_SERVER_PORT",5672),
    virtual_host='/',credentials=credentials))
channel = connection.channel()
# 在producer和consumer中分别声明⼀次以保证所要使⽤的exchange存在
channel.exchange_declare(exchange='direct_logs',exchange_type='direct')

result = channel.queue_declare(queue='cjavapy_d',durable=True)# ,arguments={"x-queue-type": "stream"})
# ⽤于获取临时queue的name
queue_name = result.method.queue
# Exchange就是根据这个RoutingKey和当前Exchange所有绑定的BindingKey做匹配，
channel.queue_bind(exchange='direct_logs',
                   queue=queue_name,
                   routing_key='info')#,arguments={"x-queue-type": "stream"})

print(' [*] Waiting for logs. To exit press CTRL+C')
# 定义⼀个回调函数来处理消息队列中的消息，
def callback(ch, method, properties, body):
# ⼿动发送确认消息
    print(" [x] %r:%r" % (method.routing_key, body))
    ch.basic_ack(delivery_tag=method.delivery_tag)
print(queue_name)
channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue=queue_name,on_message_callback=callback)
# 开始接收信息，并进⼊阻塞状态，队列⾥有信息才会调⽤callback进⾏处理
channel.start_consuming()