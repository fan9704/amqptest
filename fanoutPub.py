import pika
import os 
connection = pika.BlockingConnection(pika.ConnectionParameters(
    host=os.getenv("AMQP_SERVER_IP",'localhost'),
    port=os.getenv("AMQP_SERVER_PORT",5672)
    ))
channel = connection.channel()
channel.exchange_declare(exchange='broadcast', exchange_type='fanout')
message = 'big bang!'
channel.basic_publish(
    exchange='broadcast',
    routing_key='',
    body=message,
)
print("[v] Send %r" % message)
connection.close()