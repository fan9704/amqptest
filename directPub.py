import os
import pika
import sys

credentials = pika.PlainCredentials('guest', 'guest') # mq⽤户名和密码
connection = pika.BlockingConnection(pika.ConnectionParameters(
    host=os.getenv("AMQP_SERVER_IP",'localhost'),
    port=os.getenv("AMQP_SERVER_PORT",5672),
    virtual_host='/',
    credentials=credentials
    ))
channel = connection.channel()
# 声明⼀个名为direct_logs的direct类型的exchange
channel.exchange_declare(exchange='direct_logs',
                         exchange_type='direct')

severity = ['info', 'warning', 'error']
for i in range(20):
    message = '{} Hello World! {}'.format(i, severity[i % 3])
    # 向名为direct_logs的exchage按照设置的routing_key发送messag
    channel.basic_publish(exchange='direct_logs',
                          routing_key=severity[i % 3],
                          body=message)
    print(" [x] Sent: {}".format(message))
connection.close()